/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame.State;

import com.jme3.app.Application;
import com.jme3.app.SimpleApplication;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetManager;
import com.jme3.bounding.BoundingBox;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.input.ChaseCamera;
import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;


/**
 *
 * @author Logan and Ameythyst
 */
public class Level01State extends AbstractAppState {

    private final Node rootNode;
    private final Node localRootNode = new Node("Level 1");
    private final AssetManager assetManager;
    private final InputManager inputManager;
    private BulletAppState bulletAppState;
    private Spatial player;
    private CharacterControl playerControl;
    private final Camera camera;
    private ChaseCamera chaseCam;

    
    private final Vector3f playerWalkDirection = Vector3f.ZERO;
    private boolean left = false, right = false, up = false, down = false;
    private boolean firstPerson;
    private boolean counter;
    
    public Level01State(SimpleApplication app) {
        this.actionListener = new ActionListener() {
            @Override
            public void onAction(String name, boolean KeyPressed, float tpf){
                if(name.equals("Pause") && !KeyPressed){
                    setEnabled(!isEnabled());
                }
                else if(name.equals("Up")){
                    up = KeyPressed;
                }
                else if(name.equals("Down")){
                    down = KeyPressed;
                }
                else if(name.equals("Left")){
                    left = KeyPressed;
                }
                else if(name.equals("Right")){
                    right = KeyPressed;
                }
                else if(name.equals("Jump")){
                    Vector3f PLayerJumpDirection = new Vector3f(0,10,0);
                    playerControl.jump(PLayerJumpDirection);
                }
                else if(name.equals("camSwap")){
                    if(firstPerson){

                                if(counter) {   
                                    chaseCam.setMinDistance(10.0f);
                                    chaseCam.setMaxDistance(10.0f);
                                    firstPerson = false;
                                    counter = false;
                                }else {
                                    counter = true;
                                }
                    }
                    else{
                                if(counter) {   
                                    chaseCam.setMinDistance(0.1f);
                                    chaseCam.setMaxDistance(0.1f);
                                    firstPerson = true;
                                    counter = false;
                                }else {
                                    counter = true;
                                }
                    }
                }
                
            }
            
        };
        rootNode = app.getRootNode();
        assetManager = app.getAssetManager();
        inputManager = app.getInputManager();
        camera = app.getCamera();
        
    }

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        
        bulletAppState = new BulletAppState();
        bulletAppState.setDebugEnabled(true);
        stateManager.attach(bulletAppState);
                
        
        rootNode.attachChild(localRootNode);


        Spatial scene = assetManager.loadModel("Scenes/IntroWorld.j3o");
        localRootNode.attachChild(scene);
        
        //Load floor and control
        Spatial floor = localRootNode.getChild("Floor");
        bulletAppState.getPhysicsSpace().add(floor.getControl(RigidBodyControl.class));
        bulletAppState.getPhysicsSpace().setGravity(new Vector3f(0,-10,0));
        
        //load player
        player = localRootNode.getChild("Player");
        BoundingBox boundingBox = (BoundingBox) player.getWorldBound();
        float radius = boundingBox.getXExtent();
        float height = boundingBox.getYExtent();
        
        
        
        CapsuleCollisionShape playerShape = new CapsuleCollisionShape(radius, height);
        
                
        playerControl = new CharacterControl(playerShape, 1.0f);
        player.addControl(playerControl);
        playerControl.setGravity(new Vector3f(0,-10,0));
        
        
        bulletAppState.getPhysicsSpace().add(playerControl);
        
        inputManager.addMapping("Pause", new KeyTrigger(KeyInput.KEY_P));
        inputManager.addMapping("Up", new KeyTrigger(KeyInput.KEY_W));
        inputManager.addMapping("Down", new KeyTrigger(KeyInput.KEY_S));
        inputManager.addMapping("Left", new KeyTrigger(KeyInput.KEY_A));
        inputManager.addMapping("Right", new KeyTrigger(KeyInput.KEY_D));
        inputManager.addMapping("Jump", new KeyTrigger(KeyInput.KEY_SPACE));
        inputManager.addMapping("camSwap", new KeyTrigger(KeyInput.KEY_T));
        inputManager.addListener(actionListener, "Pause", "Up", "Down", "Left", "Right", "Jump", "camSwap");

        chaseCam = new ChaseCamera(camera, localRootNode.getChild("Player"), inputManager);
        chaseCam.setMinDistance(0.1f);
        chaseCam.setMaxDistance(0.1f);
        chaseCam.setInvertVerticalAxis(true);
        chaseCam.setDragToRotate(false);
        firstPerson = true;
        
    }
    
    private final ActionListener actionListener;
    
    @Override
    public void cleanup(){
        rootNode.detachChild(localRootNode);

        super.cleanup();
    }

    @Override
    public void update(float tpf) {
        Vector3f camDir = camera.getDirection().clone();
        Vector3f camLeft = camera.getLeft().clone();
        camDir.y = 0;
        camLeft.y = 0;
        
        camDir.normalizeLocal();
        camLeft.normalizeLocal();
        
        playerWalkDirection.set(0, 0, 0);
        
        if(left) playerWalkDirection.addLocal(camLeft);
        if(right) playerWalkDirection.addLocal(camLeft.negate());
        if(up) playerWalkDirection.addLocal(camDir);
        if(down) playerWalkDirection.addLocal(camDir.negate());
        
       if(player != null) {
           playerWalkDirection.multLocal(100).multLocal(tpf);
           playerControl.setWalkDirection(playerWalkDirection);
       }
    }
}